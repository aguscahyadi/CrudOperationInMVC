﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DMS.Common
{
    public static class Extension
    {
        public static SelectList AsSelectList<T>(this IEnumerable<T> dataList, Func<T, String> nameSelector, Func<T, String> valueSelector) where T : class
        {
            IEnumerable<NameValueItem> nameValueList = dataList.Select(data => new NameValueItem(nameSelector(data), valueSelector(data)));
            return new SelectList(Enumerable.Repeat(NameValueItem.Empty, 1).Concat(nameValueList), NameValueItem.ValuePorperty, NameValueItem.NamePorperty, NameValueItem.Empty);
        }
    }
}
