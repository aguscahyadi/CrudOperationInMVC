﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.IO;
using System.Globalization;
using System.Data.SqlClient;
using UOBISecurity;
using System.Data;
using System.ComponentModel;
using DMS.Models;

namespace DMS.Common
{

    public class Function
    {
        public static DataTable ConvertListDataTable<T>(IEnumerable<T> list)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dt = new DataTable();
            
            for (int i = 0; i < properties.Count; i++) {
                PropertyDescriptor property = properties[i];
                dt.Columns.Add(property.Name, property.PropertyType);
            }

            object[] values = new object[properties.Count];
            foreach (T item in list) {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = properties[i].GetValue(item);
                }
                dt.Rows.Add(values);
            }
            return dt;
        } 

        public static string getParam(string Param_CD, string Param_SubCD) 
        {
            try
            {
                SQLExecute execute = new SQLExecute();
                DataSet ds = new DataSet();
                dynamic args = new
                {
                    Param_CD = Param_CD,
                    Param_SubCD = Param_SubCD
                };
                ds = execute.StoredProcedureDataSet("sp_getParam", args);
                return ds.Tables[0].Rows[0]["value"].ToString();
            }
            catch(Exception ex)
            {
                Helper.LoggingError(ex);
                return "";
            }
        }

        public static Int32 GetInt(String value) {
            try {
                return Convert.ToInt32(value);
            } 
            catch (Exception ex) {
                Helper.LoggingError(ex);
                return 0;
            }   
        }

        public IList<CommonModel> GetColumnList(String tableName, String ProductID = "")
        {
            try
            {
                SQLExecute execute = new SQLExecute();
                List<CommonModel> ColumnList = new List<CommonModel>();

                DataSet ds = new DataSet();
                dynamic args = new
                {
                    tableName = tableName,
                    ProductID = ProductID
                };
                ds = execute.StoredProcedureDataSet("sp_getColumnList", args);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    ColumnList.Add(new CommonModel{ Column_name = ds.Tables[0].Rows[i]["Column_name"].ToString(), Column_Field_name = ds.Tables[0].Rows[i]["Column_Field_name"].ToString() });
                }
                return ColumnList;
            }
            catch (Exception ex)
            {
                Helper.LoggingError(ex);
                return null;
            }
        }

        public IList<String> GetParamGlobalList(String ParameterCode)
        {
            try
            {
                SQLExecute execute = new SQLExecute();
                List<String> ColumnList = new List<String>();

                DataSet ds = new DataSet();
                dynamic args = new
                {
                    Param_CD = ParameterCode
                };
                ds = execute.StoredProcedureDataSet("sp_getParamGlobal", args);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ColumnList.Add(ds.Tables[0].Rows[i]["Code"].ToString());
                }
                return ColumnList;
            }
            catch (Exception ex)
            {
                Helper.LoggingError(ex);
                return null;
            }
        }

        public IList<String> GetConditionList()
        {
            try
            {
                List<String> conditionList = new List<String>();
                conditionList.Add("=");
                conditionList.Add("!=");
                conditionList.Add(">");
                conditionList.Add(">=");
                conditionList.Add("<");
                conditionList.Add("<=");
                conditionList.Add("Like");
                conditionList.Add("Not Like");
                return conditionList;
            }
            catch (Exception ex)
            {
                Helper.LoggingError(ex);
                return null;
            }
        }

        public static Double GetDouble(String value) {
            try {
                return Convert.ToDouble(value, CultureInfo.GetCultureInfo("id-ID"));
            } 
            catch (Exception ex) {
                Helper.LoggingError(ex);
                return 0;
            }
        }

        public static Decimal GetDecimal(String value) {
            try {
                return Convert.ToDecimal(value, CultureInfo.GetCultureInfo("id-ID"));
            } 
            catch (Exception ex) {
                Helper.LoggingError(ex);
                return 0;
            }
        }

        public static Boolean GetBoolean(int value) {
            try {
                return Convert.ToBoolean(value);
            } 
            catch (Exception ex) {
                Helper.LoggingError(ex);
                return false;
            }
        }

        public static DateTime GetDate(String value) {
            try {
                return Convert.ToDateTime(value);
            } 
            catch (Exception ex) {
                Helper.LoggingError(ex);
                return new DateTime();
            }
        }

        public static DateTime GetMinSQLDate() {
            return new DateTime();
        }

        public static String GetDateDisplay(DateTime value) {
            try {
                return value.ToString("dd-MMM-yyyy");
            } 
            catch (Exception ex) 
            {
                Helper.LoggingError(ex);
                return new DateTime().ToString("dd-MMM-yyyy");
            }
        }

        public static String GetStringDateDisplay(DateTime value) {
            try {
                return value.ToString("dd/MM/yyyy");
            } 
            catch (Exception ex) 
            {
                Helper.LoggingError(ex);
                return GetMinSQLDate().ToString("dd/MM/yyyy");
            }
        }

        public static String GetSqlStringDate(DateTime value) {
            try {
                return value.ToString("yyyy-MM-dd");
            } 
            catch (Exception ex) {
                Helper.LoggingError(ex);
                return GetMinSQLDate().ToString("yyyy-MM-dd");
            }
        }

        public static String GetSqlStringDateTime(DateTime value) {
            try {
                return value.ToString("yyyy-MM-dd HH:mm:ss");
            } 
            catch (Exception ex) {
                Helper.LoggingError(ex);
                return GetMinSQLDate().ToString("yyyy-MM-dd HH:mm:ss");
            }
        }
    }
}
