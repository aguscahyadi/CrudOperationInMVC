﻿using DMS.ViewModel;
using System;

using System.Diagnostics;

namespace DMS.Common
{ 
    public class Helper 
    {
        public static int TotalPage(int listCount, SearchViewModel searchParam)
        {
            return (Convert.ToInt32(Math.Ceiling(Convert.ToDouble(listCount) / Convert.ToDouble(searchParam.pageSize))) == 0) ? 1 :
                            Convert.ToInt32(Math.Ceiling(Convert.ToDouble(listCount) / Convert.ToDouble(searchParam.pageSize)));
        }

        public static void LoggingError(Exception ex, string optionalMessage = "")
        {
            
            EventLog.WriteEntry("DMS exception", 
                string.Format("Message:\n{0}\nStackTrace:\n{1}\nOptional Message:\n{2}", ex.Message, ex.StackTrace, optionalMessage), 
                EventLogEntryType.Error, 121, short.MaxValue);
        }
    }
}