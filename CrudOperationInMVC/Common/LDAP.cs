﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.DirectoryServices;

namespace DMS.Common
{
    public class LDAP
    {        
        String _path = "";
        String _filterAttribute;

        public Boolean IsAuthenticated(String domain, String path, String username, String pwd, ref string msg) {
            String domainAndUsername = domain + @"\" + username;
            DirectoryEntry entry = new DirectoryEntry(path, domainAndUsername, pwd, AuthenticationTypes.Secure);

            try {
                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(sAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (result == null) {
                    return false;
                }

                //Update the new path to the user in the directory.
                _path = result.Path;
                _filterAttribute = result.Properties["cn"][0].ToString();
            }
            catch (Exception ex) {
                msg = ex.Message;
                Helper.LoggingError(ex, username);
                //Throw New Exception("Error authenticating user. " & ex.Message)
                return false;
            }

            return true;
        }

        public String GetGroups()
        {
            DirectorySearcher search = new DirectorySearcher(_path);
            search.Filter = "(cn=" + _filterAttribute + ")";
            search.PropertiesToLoad.Add("memberOf");
            StringBuilder groupNames = new StringBuilder();

            try
            {
                SearchResult result = search.FindOne();
                int propertyCount = result.Properties["memberOf"].Count;

                String dn;
                int equalsIndex, commaIndex;

                int propertyCounter;

                for (propertyCounter = 0; propertyCounter <= propertyCount - 1; propertyCounter++)
                {
                    dn = result.Properties["memberOf"][propertyCounter].ToString();

                    equalsIndex = dn.IndexOf("=", 1);
                    commaIndex = dn.IndexOf(",", 1);
                    if (equalsIndex == -1)
                    {
                        return null;
                    }

                    groupNames.Append(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1));
                    groupNames.Append("|");
                }
            }
            catch (Exception ex)
            {
                Helper.LoggingError(ex);
            }
            return groupNames.ToString();
        }
    }
}
