﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using UOBISecurity;
using System.Configuration;
using Microsoft.CSharp;
using System.Globalization;
using System.Threading;
using System.Reflection;
using System.Diagnostics;

namespace DMS.Common
{
    public class MSSQL
    {
        //protected readonly mssql As cMSSQL
        protected String userID;
        protected String moduleID;

        public String _UserID;
        public String _Server;
        public String _Database;
        public String _DataSource;
        public String _Password;
        public int _TimeOut;

        private const String SQL_SERVER_PROVIDER = "System.Data.SqlClient";
        private const String DEFAULT_DBCONFIG_NAME = "DBConfig";

        private String appConfigName = DEFAULT_DBCONFIG_NAME;
        private Security security;

        public DbConnection Connection;
        public DbProviderFactory providerFactory;

        private string _connectionString;
        
        private void InitCulture() {
            CultureInfo culture;
            culture = CultureInfo.CreateSpecificCulture("id-ID");
            Thread.CurrentThread.CurrentUICulture = culture;
            Thread.CurrentThread.CurrentCulture = culture;
        }

        #region "Connection Handling"

        private class UOBIConnectionString {
        public String server;
        public String database;
        public String user;
        public String password;
        public int timeout;
        public String cfgMode;

        public override String ToString() {
            return string.Format("cfgmode={5};UserID={2};Server={0};DBName={1};Source={0};password={3};Timeout={4};globaltransact=", server, database, user, password, timeout, cfgMode);
        }

        public String ToConnectionString() {
            return string.Format("workstation id={0};packet size=4096;user id={2};data source={0};persist security info=True;initial catalog={1};password={3}", server, database, user, password);
        }
    }

    public void New(String pConfigName, String pUserName, String pModuleId) {
        appConfigName = DEFAULT_DBCONFIG_NAME;
        userID = pUserName;
        moduleID = pModuleId;
        security = new Security();
    }

    public void New(String pUserName, String pModuleId) {
        this.New(DEFAULT_DBCONFIG_NAME, pUserName, pModuleId);        
    }

    public void InitDatabase(bool openConnection = true) {
        String encryptMode = ConfigurationManager.AppSettings.Get("EncryptMode");
        
        if (string.IsNullOrEmpty(encryptMode)) {
            InitDatabaseRegistry(openConnection);
        }
        else
        {
            if (encryptMode == "REGISTRY")
            {
                InitDatabaseRegistry();
            }
            else if (encryptMode == "NORMAL")
            {
                InitDatabaseNormal();
            }
        }
    }

    private static String GetHalfKeyFromRegistry(String appName) {
        ApplicationRegistryHandler appReg = new ApplicationRegistryHandler();
        return appReg.ReadFromRegistry("Software\\" + appName, "Key");
    }

    public static String GetWebConfigValue(String configName) {
        Encryptor encryptor = new Encryptor();
        String encryptedConfig = ConfigurationManager.AppSettings.Get(configName).ToString();
        String appName = ConfigurationManager.AppSettings.Get("AppName").ToString().Trim();
        String uobiKey = ConfigurationManager.AppSettings.Get("UOBIKey").ToString().Trim();
        String regKey = GetHalfKeyFromRegistry(appName);
        String decryptedConfig = "";
        try
        {
            decryptedConfig = encryptor.Decrypt(encryptedConfig, regKey, uobiKey);
        }
        catch (Exception ex)
        {
            EventLog.WriteEntry("DMS Common", ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
            Console.WriteLine("DBConnHandler.SetConnDetailsFromConfig. Error in setup db connection details. Error Message : " + ex.Message);
            Environment.Exit(20);
        }
        return decryptedConfig;
    }

    private void InitDatabaseRegistry(bool openConnection = true) {
        try {
            String registryDbConfig = GetWebConfigValue(appConfigName);
            _connectionString = FormatConnectionString(registryDbConfig);

            //_connectionString = "workstation id=ATHREES; packet size=4096; user id=sa;data source=ATHREES;persist security info=True;initial catalog=DMS_V2_DEV;password=bismIllahi";
            //_connectionString = _connectionString + ";Connect Timeout = 600;Min Pool Size=0;Max Pool Size=500;Pooling=True";

            _connectionString = _connectionString + ";Connect Timeout = 7200;Min Pool Size=0;Max Pool Size=1000;Pooling=True";
            if (openConnection) OpenConnection(_connectionString);
        }
        catch (Exception ex) {
            EventLog.WriteEntry("DMS Common", ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
            Console.WriteLine("DBConnHandler.SetConnDetailsFromConfig. Error in setup db connection details. Error Message : " + ex.Message);            
            Environment.Exit(20);
        }
    }
    
    private void InitDatabaseNormal() {
        try {
            String NormalDbConfig = security.DecryptTripleDes(System.Configuration.ConfigurationManager.AppSettings.Get(appConfigName), false);
            String connectionString = FormatConnectionString(NormalDbConfig);
            OpenConnection(connectionString);
        }
        catch (Exception ex) {
            EventLog.WriteEntry("DMS Common", ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
            Console.WriteLine("DBConnHandler.SetConnDetailsFromConfig. Error in setup db connection details. Error Message : " + ex.Message);            
            Environment.Exit(20);
        }
    }

    protected void OpenConnection(String connectionString) {
        OpenConnection(SQL_SERVER_PROVIDER, connectionString);
    }

    protected void OpenConnection(String provider, String connectionString) {
        try {
            if (Connection != null) {
                CloseConnection();
            }
            providerFactory = DbProviderFactories.GetFactory(provider);
            Connection = providerFactory.CreateConnection();
            if (Connection == null) {
            }

            Connection.ConnectionString = connectionString;
            Connection.Open();
        }
        catch (Exception ex) {
            EventLog.WriteEntry("DMS Common", ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
        }
   }

    public void CloseConnection() {
        try {
            if (Connection != null) {
                if (Connection.State != ConnectionState.Closed) {
                    Connection.Close();
                    Connection.Dispose();
                    Connection = null;
                }
            }
            }
            catch (Exception ex) {
                EventLog.WriteEntry("DMS Common", ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
            }
    }

    private String FormatConnectionString(String decryptedDbConfigString) {
        String[] splittedDbConfigString = decryptedDbConfigString.Split(';');
        UOBIConnectionString connectionString = new UOBIConnectionString();
        connectionString.server = GetDbConfigValue(splittedDbConfigString, "Server");
        connectionString.database = GetDbConfigValue(splittedDbConfigString, "DBName");
        connectionString.user = GetDbConfigValue(splittedDbConfigString, "UserID");
        connectionString.password = GetDbConfigValue(splittedDbConfigString, "Password");
        if (connectionString.password == "") {
            connectionString.password = GetDbConfigValue(splittedDbConfigString, "password");
        }
        connectionString.timeout = Convert.ToInt32(GetDbConfigValue(splittedDbConfigString, "Timeout")) == 0 ? 600 : Convert.ToInt32(GetDbConfigValue(splittedDbConfigString, "Timeout"));
        _TimeOut = connectionString.timeout;
        connectionString.cfgMode = GetDbConfigValue(splittedDbConfigString, "cfgmode");

        return connectionString.ToConnectionString();
    }

    private String GetDbConfigValue(String[] dbConfigList, String dbConfigKey)
    {
        foreach (String dbConfigItem in dbConfigList)
        {
            if (dbConfigItem.Contains(dbConfigKey))
            {
                return dbConfigItem.Split('=')[1];
            }
        }
        return String.Empty;
    }

    public void Create() {
        SwitchDBToAplication();
    }

    public void SwitchDBToAplication()
    {
        InitDatabase();
    }

    public string ConnectionString { get { return _connectionString; } }

#endregion

    }
}
