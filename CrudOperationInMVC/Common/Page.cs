﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DMS.Common
{
    public sealed class Page
    {
        public const String GridPagination = "~/Views/Shared/Common/_GridPagination.aspx";
        public const String Upload = "~/Views/Shared/Common/_Upload.aspx";
        public const String UploadLog = "~/Views/Shared/Common/_UploadLog.aspx";
        public const String SubGridPagination = "~/Views/Shared/Common/_SubGridPagination.aspx";
        public const String SearchForm = "~/Views/Shared/Common/_SearchForm.aspx";
        public const String SearchFormTrace = "~/Views/Shared/Common/_SearchFormTrace.aspx";
        public const String SearchTxDateForm = "~/Views/Shared/Common/_SearchTxDateForm.aspx";
        public const String UpdateInformation = "~/Views/Shared/Common/_UpdateInformation.aspx";
    }
}
