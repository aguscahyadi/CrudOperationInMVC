﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Reflection;
using System.Data.SqlClient;
using System.Diagnostics;

namespace DMS.Common
{
    public class SQLExecute
    {
        private MSSQL _con;

        public SQLExecute()
        {
            _con = new MSSQL();
        }
       
        public DataSet StoredProcedureDataSet(string _StoredProcedure, dynamic _Parameter = null) {
            try
            {
                DataSet _DataSet = new DataSet();
                _con.InitDatabase();

                using (DbCommand cmd = _con.Connection.CreateCommand())
                {
                    cmd.CommandText = _StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    if (_Parameter != null) {
                        var properties = _Parameter.GetType().GetProperties();
                        foreach (PropertyInfo property in properties)
                        {
                            if (property.GetValue(_Parameter, null) != null)
                            {
                                SqlParameter parameter = new SqlParameter();
                                parameter.ParameterName = property.Name;
                                parameter.Value = property.GetValue(_Parameter, null);
                                cmd.Parameters.Add(parameter);
                            }
                        }
                    }

                    if (_con._TimeOut > 0)
                        cmd.CommandTimeout = _con._TimeOut;
                    else  
                        cmd.CommandTimeout = 0;

                    using (DbDataAdapter da = _con.providerFactory.CreateDataAdapter()) {
                        da.SelectCommand = cmd;
                        da.Fill(_DataSet);
                    }
                }
                return _DataSet;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("DMS Common", _StoredProcedure + " \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                throw ex;
            }
            finally
            {
                _con.CloseConnection();
            }
        }

        public String SQLBulkInsert(string _tableName, DataTable _Data = null)
        {
            try
            {
                _con.InitDatabase(false);
                //using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con.Connection.ConnectionString, SqlBulkCopyOptions.CheckConstraints))
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(_con.ConnectionString, SqlBulkCopyOptions.CheckConstraints))
                {
                    bulkCopy.DestinationTableName = _tableName;
                    bulkCopy.WriteToServer(_Data);
                }
                return "Success: Insert Delivery Process [" + _Data.Rows.Count.ToString() + "] Records ";
            }
            catch (Exception ex) {
                EventLog.WriteEntry("DMS Common", _tableName + "\n " + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                throw ex;
            }
            finally
            {
                _con.CloseConnection();
            }
        }

        public int StoredProcedureSaveUpdate(string _StoredProcedure, dynamic _Parameter = null)
        {
            try
            {
                _con.InitDatabase();

                using (DbCommand cmd = _con.Connection.CreateCommand())
                {
                    cmd.CommandText = _StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (_Parameter != null)
                    {
                        var properties = _Parameter.GetType().GetProperties();
                        foreach (PropertyInfo property in properties)
                        {
                            if (property.GetValue(_Parameter, null) != null)
                            {
                                SqlParameter parameter = new SqlParameter();
                                parameter.ParameterName = property.Name; 
                                if (property.Name == "user")
                                {
                                    parameter.Value = property.GetValue(_Parameter, null).UserID;
                                }
                                else
                                {
                                    parameter.Value = property.GetValue(_Parameter, null);
                                }
                                cmd.Parameters.Add(parameter);
                            }
                        }                                     
                    }

                    if (_con._TimeOut > 0)
                        cmd.CommandTimeout = _con._TimeOut;
                    else
                        cmd.CommandTimeout = 0;

                    return cmd.ExecuteNonQuery();
                }                
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("DMS Common", _StoredProcedure + "\n " + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                throw ex;
            }
            finally
            {
                _con.CloseConnection();
            }
        }
               
        public DataSet QuerySelect(string _query)
        {
            try
            {
                DataSet _DataSet = new DataSet();
                _con.InitDatabase();

                using (DbCommand cmd = _con.Connection.CreateCommand())
                {
                    cmd.CommandText = _query;
                    cmd.CommandType = CommandType.Text;

                    if (_con._TimeOut > 0)
                        cmd.CommandTimeout = _con._TimeOut;
                    else
                        cmd.CommandTimeout = 0;

                    using (DbDataAdapter da = _con.providerFactory.CreateDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(_DataSet);
                    }
                }
                return _DataSet;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("DMS Common", _query+" \n"+ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                throw ex;
            }
            finally
            {
                _con.CloseConnection();
            }
        }


        public int QueryInsertUpdate(string _query)
        {
            try
            {
                _con.InitDatabase();

                using (DbCommand cmd = _con.Connection.CreateCommand())
                {
                    cmd.CommandText = _query;
                    cmd.CommandType = CommandType.Text;

                    if (_con._TimeOut > 0)
                        cmd.CommandTimeout = _con._TimeOut;
                    else
                        cmd.CommandTimeout = 0;

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("DMS Common", _query + " \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                throw ex;
            }
            finally
            {
                _con.CloseConnection();
            }
        }

        public String StoredProcedureDataString(string _StoredProcedure, dynamic _Parameter = null)
        {
            try
            {
                _con.InitDatabase();

                using (DbCommand cmd = _con.Connection.CreateCommand())
                {
                    cmd.CommandText = _StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (_Parameter != null)
                    {
                        var properties = _Parameter.GetType().GetProperties();
                        foreach (PropertyInfo property in properties)
                        {
                            if (property.GetValue(_Parameter, null) != null)
                            {
                                SqlParameter parameter = new SqlParameter();
                                parameter.ParameterName = property.Name;
                                if (property.Name == "user")
                                {
                                    parameter.Value = property.GetValue(_Parameter, null).UserID;
                                }
                                else
                                {
                                    parameter.Value = property.GetValue(_Parameter, null);
                                }
                                cmd.Parameters.Add(parameter);
                            }
                        }
                    }

                    if (_con._TimeOut > 0)
                        cmd.CommandTimeout = _con._TimeOut;
                    else
                        cmd.CommandTimeout = 0;

                    return cmd.ExecuteScalar().ToString();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("DMS Common", _StoredProcedure + "\n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                throw ex;
            }
            finally
            {
                _con.CloseConnection();
            }
        }

        public dynamic StoredProcedureDataResultView(string _StoredProcedure, dynamic _Parameter = null)
        {
            try
            {
                _con.InitDatabase();

                using (DbCommand cmd = _con.Connection.CreateCommand())
                {
                    cmd.CommandText = _StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (_Parameter != null)
                    {
                        var properties = _Parameter.GetType().GetProperties();
                        foreach (PropertyInfo property in properties)
                        {
                            if (property.GetValue(_Parameter, null) != null)
                            {
                                SqlParameter parameter = new SqlParameter();
                                parameter.ParameterName = property.Name;
                                if (property.Name == "user")
                                {
                                    parameter.Value = property.GetValue(_Parameter, null).UserID;
                                }
                                else
                                {
                                    parameter.Value = property.GetValue(_Parameter, null);
                                }

                                cmd.Parameters.Add(parameter);
                            }
                        }

                        cmd.Parameters.Add(new SqlParameter
                        {
                            ParameterName = "@Error",
                            SqlDbType = SqlDbType.VarChar,
                            Size = 8000,
                            Direction = ParameterDirection.Output,
                            Value = "0"
                        });
                    }

                    if (_con._TimeOut > 0)
                        cmd.CommandTimeout = _con._TimeOut;
                    else
                        cmd.CommandTimeout = 0;

                    var execResult = cmd.ExecuteScalar();
                    var result = execResult.ToString();

                    if (result == "-1" && cmd.Parameters["@Error"].Value != null && !DBNull.Value.Equals(cmd.Parameters["@Error"].Value))
                    {
                        var errorMessage = (string)cmd.Parameters["@Error"].Value;

                        throw new Exception(errorMessage);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("DMS Common", _StoredProcedure + "\n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                throw ex;
            }
            finally
            {
                _con.CloseConnection();
            }
        }

        public void StoredProcedureDataNoReturn(string _StoredProcedure, dynamic _Parameter = null)
        {
            try
            {
                _con.InitDatabase();

                using (DbCommand cmd = _con.Connection.CreateCommand())
                {
                    cmd.CommandText = _StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (_Parameter != null)
                    {
                        var properties = _Parameter.GetType().GetProperties();
                        foreach (PropertyInfo property in properties)
                        {
                            if (property.GetValue(_Parameter, null) != null)
                            {
                                SqlParameter parameter = new SqlParameter();
                                parameter.ParameterName = property.Name;
                                if (property.Name == "user")
                                {
                                    parameter.Value = property.GetValue(_Parameter, null).UserID;
                                }
                                else
                                {
                                    parameter.Value = property.GetValue(_Parameter, null);
                                }

                                cmd.Parameters.Add(parameter);
                            }
                        }

                        cmd.Parameters.Add(new SqlParameter
                        {
                            ParameterName = "@Error",
                            SqlDbType = SqlDbType.VarChar,
                            Size = 8000,
                            Direction = ParameterDirection.Output,
                            Value = "0"
                        });
                    }

                    if (_con._TimeOut > 0)
                        cmd.CommandTimeout = _con._TimeOut;
                    else
                        cmd.CommandTimeout = 0;

                    var execResult = cmd.ExecuteNonQuery();
                    var result = execResult.ToString();

                    if (result == "-1" && !DBNull.Value.Equals(cmd.Parameters["@Error"].Value))
                    {
                        var errorMessage = (string)cmd.Parameters["@Error"].Value;

                        throw new Exception(errorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("DMS Common", _StoredProcedure + "\n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                throw ex;
            }
            finally
            {
                _con.CloseConnection();
            }
        }

        public int ExecuteScalar(string query)
        {
            try
            {
                _con.InitDatabase();

                using (DbCommand cmd = _con.Connection.CreateCommand())
                {
                    cmd.CommandText = query;

                    if (_con._TimeOut > 0)
                        cmd.CommandTimeout = _con._TimeOut;
                    else
                        cmd.CommandTimeout = 0;

                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("DMS Common", query + "\n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                throw ex;
            }
            finally
            {
                _con.CloseConnection();
            }
        }

        public DataSet ExecQuery(string _StoredProcedure, dynamic _Parameter = null)
        {
            DataSet _DataSet = new DataSet();
            _con.InitDatabase(false);

            try
            {
                _con.providerFactory = DbProviderFactories.GetFactory("System.Data.SqlClient");
                using(_con.Connection = _con.providerFactory.CreateConnection()){
                    _con.Connection.ConnectionString = _con.ConnectionString;
                    _con.Connection.Open();
                    using (DbCommand cmd = _con.Connection.CreateCommand())
                    {
                        cmd.CommandText = _StoredProcedure;
                        cmd.CommandType = CommandType.StoredProcedure;

                        if (_Parameter != null)
                        {
                            var properties = _Parameter.GetType().GetProperties();
                            foreach (PropertyInfo property in properties)
                            {
                                if (property.GetValue(_Parameter, null) != null)
                                {
                                    SqlParameter parameter = new SqlParameter();
                                    parameter.ParameterName = property.Name;
                                    parameter.Value = property.GetValue(_Parameter, null);
                                    cmd.Parameters.Add(parameter);
                                }
                            }
                        }

                        if (_con._TimeOut > 0)
                            cmd.CommandTimeout = _con._TimeOut;
                        else
                            cmd.CommandTimeout = 0;

                        using (DbDataAdapter da = _con.providerFactory.CreateDataAdapter())
                        {
                            da.SelectCommand = cmd;
                            da.Fill(_DataSet);
                        }
                    }
                    return _DataSet;
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("DMS Common", _StoredProcedure + "\n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error, 121, short.MaxValue);
                throw ex;
            }
            finally {
                _con.CloseConnection();
            }
        }
    }
}
