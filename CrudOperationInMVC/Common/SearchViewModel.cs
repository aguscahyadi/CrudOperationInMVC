﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DMS.ViewModel
{
    public class SearchViewModel
    {
        public SearchViewModel() 
        {
            currentPage = 1;
            pageSize = 10;
        }

        public string variable { get; set; }
        public string operators { get; set; }
        public string value { get; set; }

        public int currentPage { get; set; }
        public int pageSize { get; set; }
    }
}
