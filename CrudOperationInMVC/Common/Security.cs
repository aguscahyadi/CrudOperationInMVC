﻿using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace DMS.Common
{
    public class Security
    {        
        public string Copyright;
        private IMAWA2G Enc = new IMAWA2G();

        #region "IMAWA2G"
            public class IMAWA2G {
                private string ASCII  = @".?q`w~e!r@t#y$ui^op*a(s)d_f+ gh{jk-l]z:x[c}vbnmQWER|T0Y1U2I3O4P5A6S7D8F\9GHJ""KL/ZX=C<V'B%>N&M;,";
                private string RegisterName = "";
                private string ActivationCode = "";
                private string Serial = "";

                public bool ValidSerial(string SerialNum) {
                    if (Serial.Length > 0) {
                        if (Decrypt(Serial) == Decrypt(SerialNum))
                            return true;
                        else
                            return false;
                    }
                    else 
                        return false;
                }

                public bool isvalid() {
                    return true;
                }

                public string FirstGenEnc(string SrcString) {
                    if ((SrcString.Length > 0) && (isvalid() == true)) {
                        string BlockCode = "h" + "H" + "i" + "I" + "j" + "J" + "k" + "K" + "t" + "T" + "u" + "U" + "v" + "V" + "w" + "W" + "x" + "X" + "y" + "Y" + "z" + "Z" + "0" + "a" + "1" + "A" + "2" + "b" + "3" + "B" + "4" + "c" + "5" + "C" + "6" + "d" + "7" + "D" + "8" + "e" + "9" + "E" + "." + "f" + "," + "F" + "g" + "G" + "l" + "L" + "m" + "M" + "n" + "N" + "o" + "O" + "p" + "P" + "q" + "Q" + "r" + "R" + "s" + "S";
                        int i = 0;
                        int MaxRange = BlockCode.Length - 1;
                        Random Ran = new Random();
                        int RandomValue = Ran.Next(1, 44);
                        string TextResult = BlockCode[RandomValue].ToString();
                        int EncryptValue;
                    
                        for (i = 0; i < SrcString.Length - 1; i++) {
                            if (BlockCode.IndexOf(SrcString[i]) >= 0) {
                                EncryptValue = BlockCode.IndexOf(SrcString[i]) + RandomValue;
                                if (EncryptValue > MaxRange) {
                                    EncryptValue = EncryptValue - (MaxRange + 1);
                                }
                                TextResult += BlockCode[EncryptValue];
                            }
                            else
                                TextResult += SrcString[i];                            
                        }
                        //Replace
                        char RplChar = ""[0];
                        char[] ChrArray = TextResult.ToCharArray();
                        for (i = 0; i < ChrArray.GetUpperBound(0); i++) {
                            if (ChrArray[i].ToString() == "") { 
                                ChrArray[i] = "`"[0];
                            }
                        }

                        int PrevPos = -1;
                        for (i = 0; i < ChrArray.GetUpperBound(0); i++) {
                            if (i % 2 == 0) {
                                RplChar = ChrArray[i];
                                PrevPos = i;
                            }
                            else
                            {
                                if (PrevPos > -1) {
                                    ChrArray[PrevPos] = ChrArray[i];
                                    ChrArray[i] = RplChar;
                                }
                            }
                        }
                        TextResult = "";
                        for (i = 0; i < ChrArray.GetUpperBound(0); i++) {
                            TextResult += ChrArray[i];
                        }
                        return TextResult;
                    }
                    else
                        return SrcString;
                }

                public string FirstGenDec(string rRPMlJJ7MR5) {
                    if ((rRPMlJJ7MR5.Length > 0) && (isvalid() == true)) {
                        string BlockCode = "h" + "H" + "i" + "I" + "j" + "J" + "k" + "K" + "t" + "T" + "u" + "U" + "v" + "V" + "w" + "W" + "x" + "X" + "y" + "Y" + "z" + "Z" + "0" + "a" + "1" + "A" + "2" + "b" + "3" + "B" + "4" + "c" + "5" + "C" + "6" + "d" + "7" + "D" + "8" + "e" + "9" + "E" + "." + "f" + "," + "F" + "g" + "G" + "l" + "L" + "m" + "M" + "n" + "N" + "o" + "O" + "p" + "P" + "q" + "Q" + "r" + "R" + "s" + "S";
                        int i = 0;
                        int MaxRange = BlockCode.Length - 1;
                        string TextResult = "";
                        int EncryptValue;
                        string SrcString= "";
                        //Replace
                        char RplChar = ""[0];
                        char[] ChrArray = rRPMlJJ7MR5.ToCharArray();
                        for (i = 0; i < ChrArray.GetUpperBound(0); i++) {
                            if (ChrArray[i].ToString() == "`") {
                                ChrArray[i] = " "[0];
                            }
                        }
                        int PrevPos = -1;
                        for (i = 0; i < ChrArray.GetUpperBound(0); i++) {
                            if (i % 2 == 0) {
                                RplChar = ChrArray[i];
                                PrevPos = i;
                            }
                            else {
                                if (PrevPos > -1) {
                                    ChrArray[PrevPos] = ChrArray[i];
                                    ChrArray[i] = RplChar;
                                }
                            }
                        }
                        SrcString = "";
                        for (i = 0; i < ChrArray.GetUpperBound(0); i++) {
                            SrcString += ChrArray[i];
                        }
                        //De Encrypt
                        int RandomValue = BlockCode.IndexOf(SrcString[0]);
                        for (i = 1; i < SrcString.Length - 1; i++) {
                            if (BlockCode.IndexOf(SrcString[i]) >= 0) {
                                EncryptValue = BlockCode.IndexOf(SrcString[i]) - RandomValue;
                                if (EncryptValue < 0) 
                                    EncryptValue = EncryptValue + (MaxRange + 1);
                                TextResult += BlockCode[EncryptValue];
                            }
                            else
                                TextResult += SrcString[i];                            
                        }
                        return TextResult;
                    }
                    else
                        return rRPMlJJ7MR5;
                }

                public string Encrypt(string _Value) {
                    if (_Value == null) 
                        _Value = "";
                    
                    Random Ran = new Random();
                    byte First = Convert.ToByte(Ran.Next(0, ASCII.Length - 1));
                    int i = 0;
                    string _Result = ASCII[First].ToString();
                    byte Token;
                    try {
                        if ((_Value.Length > 0) && isvalid() == true) {
                            for (i = 0; i < _Value.Length - 1; i++) {
                                Token = Convert.ToByte(ASCII.IndexOf(_Value[i]) + First);
                                if (Token >= 0) {
                                    if (Token >= ASCII.Length) 
                                        Token = Convert.ToByte(Token - (ASCII.Length));
                                    _Result += ASCII[Token];
                                }
                                else
                                    _Result += _Value[i];
                            }
                            _Result = Encoding(_Result);
                        }
                        else
                            _Result = _Value;
                    }
                    catch (Exception ex) {
                        Helper.LoggingError(ex);
                    }
                    return _Result;
                }

                private string ToBit(byte bt, byte Num) {
                    byte Key = bt;
                    string Bit = "";
                    byte _Mod = 0;
                    while (Key > 0) {
                        _Mod = Convert.ToByte(Key % 2);
                        Bit = _Mod.ToString().Trim() + Bit;
                        if (Key - _Mod > 0) 
                            Key = Convert.ToByte((Key - _Mod) / 2);
                        else 
                            Key = 0;
                    }
                    
                    while (Bit.Length < Num) {
                        Bit = "0" + Bit;
                    }
                    return Bit;
                }

                private byte ToByte(string Bit) {
                    int i;
                    byte _Result = 0;
                    if (Bit.Length > 1) {
                        for (i = 0; i < Bit.Length - 2; i++) {
                            _Result += Convert.ToByte(Math.Pow(2, Bit.Length - i - 1) * Convert.ToByte(Bit[i].ToString().Trim()));
                        }
                    }
                    _Result += Convert.ToByte(Bit[Bit.Length - 1].ToString().Trim());
                    return _Result;
                }

                private string Mapping(string _Value) {
                    int i;
                    string Pola;
                    string _Result = _Value;
                    for (i = 16; i < 30; i++) {
                        Pola = (ToBit(16, 5)[1] + ToBit(16, 5)[1] + ToBit(16, 5)[2] + ToBit(16, 5)[2]).ToString();
                        Pola += ToBit(16, 5)[3] + ToBit(16, 5)[3] + ToBit(16, 5)[4] + ToBit(16, 5)[4];
                        if (_Value == Pola) 
                            _Result = Pola;
                    }
                    return _Result;
                }

                private string Encoding(string _Value) {
                    string[] _Matrix = new string[_Value.Length - 1];
                    int i, j;
                    for (i = 0; i < _Value.Length - 1; i++) {
                        _Matrix[i] = ToBit(Convert.ToByte(ASCII.IndexOf(_Value[i])), 8);
                    }
                    
                    string _result = "";
                    for (i = 0; i < 7; i++) {
                        for (j = 0; j < _Value.Length - 1; j++) {
                            _result += _Matrix[j][i];
                        }
                    }

                    Array.Resize(ref _Matrix, (_result.Length / 4) - 1);

                    j = 0;
                    _Matrix[j] = "";
                    for (i = 1; i < _result.Length; i++) {
                        if (i % 4 == 0) {
                            _Matrix[j] += _result[i - 1];
                            j += 1;
                            if (j <= (_Matrix.GetUpperBound(0)))
                                _Matrix[j] = "";
                        } 
                        else
                            if (j <= (_Matrix.GetUpperBound(0))) 
                                _Matrix[j] += _result[i - 1];
                    }
                    _result = "";
                    for (i = 0; i < _Matrix.GetUpperBound(0); i++) {
                        _result += ASCII[ToByte(_Matrix[i])];
                    }
                    return _result;
                }

                private string Decoding(string _Value) {
                    string _result = ""; 
                    try
                    {
                        int i, j;
                        for (i = 0; i <= _Value.Length - 1; i++)
                        {
                            _result += ToBit(Convert.ToByte(ASCII.IndexOf(_Value[i])), 8);
                        }

                        string[] _Matrix = new string[(_result.Length / 8)];

                        for (i = 0; i <= _Matrix.GetUpperBound(0); i++)
                        {
                            _Matrix[i] = "";
                        }

                        for (j = 0; j <= _result.Length - 1; j++)
                        {
                            i = j % (_Matrix.GetUpperBound(0) + 1);
                            _Matrix[i] += _result[j];
                        }
                        _result = "";
                        for (i = 0; i <= _Matrix.GetUpperBound(0); i++)
                        {
                            _result += ASCII[ToByte(_Matrix[i])];
                        }
                    }
                    catch (Exception ex) {
                        Helper.LoggingError(ex);
                    }
                    return _result;
                }

                public string Decrypt(string _Value) {
                    if (_Value == null) 
                        _Value = "";
                    if (isvalid() == true) 
                        return Decrypt(_Value, false);
                    else 
                        return _Value;                
                }

                private string Decrypt(string _Value, bool Init) {
                    System.Random Random = new System.Random();
                    byte First;
                    int i = 0;
                    string _Result = "";
                    int Token;
                    try {
                        if (_Value.Length > 0) {
                            _Value = Decoding(_Value);
                            First = Convert.ToByte(ASCII.IndexOf(_Value[0]));
                            for (i = 0; i <= _Value.Length - 1; i++) {
                                Token = ASCII.IndexOf(_Value[i]);
                                if (Token >= 0) {
                                    Token = Token - First;
                                    if (Token < 0) 
                                        Token = ASCII.Length + Token;
                                    _Result += ASCII[Token];
                                }
                                else 
                                    _Result += _Value[i];
                            }
                        }
                        else
                            _Result = _Value;
                    }    
                    catch (Exception) {
                    }
                    return _Result;
                }

                public void New() {
                    RegisterName = System.Configuration.ConfigurationManager.AppSettings.Get("register");
                    ActivationCode = System.Configuration.ConfigurationManager.AppSettings.Get("activation");
                    Serial = System.Configuration.ConfigurationManager.AppSettings.Get("serial");
                }
            }
#endregion

        public String DecryptTripleDes(String cipherString, Boolean useHashing) {
            try
            {
                UTF8Encoding utf8Encoding = new System.Text.UTF8Encoding(true);
                Byte[] keyArray;
                Byte[] toEncryptArray = Convert.FromBase64String(cipherString);

                String key = "!Int3rnalSupp0rt";
                if (useHashing)
                {
                    MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                    keyArray = hashmd5.ComputeHash(utf8Encoding.GetBytes(key));
                    hashmd5.Clear();
                    return null;
                }
                else
                {
                    keyArray = utf8Encoding.GetBytes(key);
                    TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                    tdes.Key = keyArray;
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    ICryptoTransform cTransform = tdes.CreateDecryptor();
                    Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                    tdes.Clear();
                    return utf8Encoding.GetString(resultArray);
                }
            }
            catch (Exception ex)
            {
                Helper.LoggingError(ex);
                return null;
            }
        }
    }
}
