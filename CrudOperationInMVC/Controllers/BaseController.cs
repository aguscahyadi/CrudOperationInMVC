﻿using CrudOperationInMVC.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrudOperationInMVC.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        protected EmployeeRepository _employeeRepository;

        public BaseController()
        {
            _employeeRepository = new EmployeeRepository();

        }
    }
}