﻿using CrudOperationInMVC.Models;
using DMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrudOperationInMVC.Controllers
{
    public class EmployeeController : BaseController
    {
        Employee _context = new Employee();
        private readonly SQLExecute _execute;

        public EmployeeController()
        {
            _execute = new SQLExecute();
        }


        public ActionResult Index()
        {
            //return View();
            //var listofData = _context.Employees.ToList();
            //ViewBag.Message = "Data Insert Successfully";
            //return View(listofData);
            dynamic args = new
            {
                RefID = 1
            };
            DataSet ds = _execute.StoredProcedureDataSet("sp_getData", args);

            var listReference = new List<Employee>();

            if (ds != null)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    var item = new Employee
                    {
                        Employeeld = Convert.ToInt32(dr["Employeeld"].ToString()),
                        EmployeeName = dr["EmployeeName"].ToString(),
                        EmployeeSalary = Convert.ToInt32(dr["EmployeeSalary"].ToString()),
                        EmployeeCity = dr["EmployeeCity"].ToString()
                    };
                    listReference.Add(item);
                }
            }

            return View(listReference); 
            //return null;
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Employee model)
        {
            // _context.Employee.Add(model);
            //_context.SaveChanges();
            // ViewBag.Message = "Data Insert Successfully";
            //return View();

            //return RedirectToAction("Employee");
            try
            {
                int process = 0;
                process = _employeeRepository.SaveProcess(model);
                if (process >= 0)
                {
                    return Json(new ResponseModel { ResponseType = ResponseModel.Error, Message = "Save Failed" });
                }
                else
                {
                    return Json(new ResponseModel { ResponseType = ResponseModel.Success, Message = "Save Success" });
                }
            }
            catch (Exception ex)
            {
               return Json(new ResponseModel { ResponseType = ResponseModel.Error, Message = "Save Failed "+ex.Message.ToString() });
            }
        }

        public ActionResult Delete(int id)
        {
            //var data = _context.Employees.Where(x => x.Employeeld == id).FirstOrDefault();
            //_context.Employees.Remove(data);
            //_context.SaveChanges();
            //ViewBag.Messsage = "Record Delete Successfully";
            //return RedirectToAction("Employee");
            var listReference = new List<Employee>();
            try
            {
                dynamic args = new
                {
                    Employeeld = 1
                };
                return _execute.StoredProcedureSaveUpdate("sp_DeleteEmployee", args);
            }
            catch (Exception ex)
            {
                Helper.LoggingError(ex);
                return View(listReference);
            } 

        }
    }
}