﻿using CrudOperationInMVC.Models;
using DMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrudOperationInMVC.Controllers
{
    public class HomeController : Controller
    {
        //TutorialsCS _context = new TutorialsCS();

        //public ActionResult Index()
        //{
        //    var listofData = _context.Employees.ToList();
        //    return View(listofData);
        //}
        Employee _context = new Employee();
        private readonly SQLExecute _execute;

        public HomeController()
        {
            _execute = new SQLExecute();
        }
        public ActionResult Index()
        {
            dynamic args = new
            {
                RefID = 1
            };
            DataSet ds = _execute.StoredProcedureDataSet("sp_getData", args);

            var listReference = new List<Employee>();

            if (ds != null)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    var item = new Employee
                    {
                        Employeeld = Convert.ToInt32(dr["Employeeld"].ToString()),
                        EmployeeName = dr["EmployeeName"].ToString(),
                        EmployeeSalary = Convert.ToInt32(dr["EmployeeSalary"].ToString()),
                        EmployeeCity = dr["EmployeeCity"].ToString()
                    };
                    listReference.Add(item);
                }
            }

            return View(listReference);
        }
      

        //[HttpPost]
        //public ActionResult Create(Employee model)
        //{
        //    _context.Employees.Add(model);
        //    _context.SaveChanges();
        //    ViewBag.Message = "Data Insert Successfully";
        //    //return View();

        //    return RedirectToAction("Employee");
        //}
        //[HttpGet]
        //public ActionResult Edit(int id)
        //{
        //    var data = _context.Employees.Where(x => x.Employeeld == id).FirstOrDefault();
        //    return View(data);
        //}
        //[HttpPost]
        //public ActionResult Edit(Employee Model)
        //{
        //    var data = _context.Employees.Where(x => x.Employeeld == Model.Employeeld).FirstOrDefault();
        //    if (data != null)
        //    {
        //        data.EmployeeCity = Model.EmployeeCity;
        //        data.EmployeeName = Model.EmployeeName;
        //        data.EmployeeSalary = Model.EmployeeSalary;
        //        _context.SaveChanges();
        //    }

        //    return RedirectToAction("Employee");
        //}
        //public ActionResult Delete(int id)
        //{
        //    var data = _context.Employees.Where(x => x.Employeeld == id).FirstOrDefault();
        //    _context.Employees.Remove(data);
        //    _context.SaveChanges();
        //    ViewBag.Messsage = "Record Delete Successfully";
        //    return RedirectToAction("Employee");
        //}
        //public ActionResult Detail(int id)
        //{
        //    var data = _context.Employees.Where(x => x.Employeeld == id).FirstOrDefault();
        //    return View(data);
        //}

        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return View();
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}

        //public ActionResult Employee()
        //{
        //    //var listofData = _context.Employees.ToList();
        //    //ViewBag.Message = "Data Insert Successfully";
        //    //return View(listofData);
        //    dynamic args = new
        //    {
        //        RefID = 1
        //    };
        //    DataSet ds = _execute.StoredProcedureDataSet("sp_getData", args);

        //    var listReference = new List<Employee>();

        //    if (ds != null)
        //    {
        //        foreach (DataRow dr in ds.Tables[0].Rows)
        //        {
        //            var item = new Employee
        //            {
        //                Employeeld = Convert.ToInt32(dr["Employeeld"].ToString()),
        //                EmployeeName = dr["EmployeeName"].ToString(),
        //                EmployeeSalary = Convert.ToInt32(dr["EmployeeSalary"].ToString()),
        //                EmployeeCity = dr["EmployeeCity"].ToString()
        //            };
        //            listReference.Add(item);
        //        }
        //    }

        //    return View(listReference); return null;
        //    }
        //}

    }
}