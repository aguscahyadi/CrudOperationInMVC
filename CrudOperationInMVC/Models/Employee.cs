using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudOperationInMVC.Models
{
    public class Employee
    {
        public int Employeeld { get; set; }
        public string EmployeeName { get; set; }
        public decimal EmployeeSalary { get; set; }
        public string EmployeeCity { get; set; }
    }
}