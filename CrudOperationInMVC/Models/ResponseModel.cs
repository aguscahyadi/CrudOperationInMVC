﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudOperationInMVC.Models
{
    public class ResponseModel
    {
        
            public const String Info = "I";
            public const String Warning = "W";
            public const String Error = "E";
            public const String Success = "S";

            public String ResponseType { get; set; }
            public String Message { get; set; }
            public String Value { get; set; }
        
    }
}