﻿using CrudOperationInMVC.Models;
using DMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudOperationInMVC.Repository
{
    public class EmployeeRepository
    {
        SQLExecute _execute;
        public EmployeeRepository()
        {
            _execute = new SQLExecute();
        }

        public int SaveProcess(Employee emp)
        {
            try
            {
                dynamic args = new
                {
                    Employeeld = emp.Employeeld,
                    EmployeeName = emp.EmployeeName,
                    EmployeeSalary = emp.EmployeeSalary,
                    EmployeeCity = emp.EmployeeCity
                };
                return _execute.StoredProcedureSaveUpdate("sp_SaveEmployee", args);
            }
            catch (Exception ex)
            {
                Helper.LoggingError(ex);
                return 0;
            }
        }
        public int DeleteProcess(Employee emp)
        {
            try
            {
                dynamic args = emp;
                return _execute.StoredProcedureSaveUpdate("sp_DeleteEmployee", args);
            }
            catch (Exception ex)
            {
                Helper.LoggingError(ex);
                return 0;
            }
        }
    }
}